package cz.cvut.fel.ts1;

public class Bornedan {
    public long factorial(int n){
        if(n<0){
            return 0;
        }else if(n==0){
            return 1;
        }
        long res = (long)n;
        for (int i = 1; i < n; i++) {
            res = res * (n-i);
        }
        return res;
    }
}
