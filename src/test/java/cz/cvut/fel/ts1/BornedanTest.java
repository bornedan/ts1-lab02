package cz.cvut.fel.ts1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class BornedanTest {

    @Test
    public void factorialTest_nIs5_retruns120() {

        Bornedan test = new Bornedan();
        long res1 = test.factorial(5);
        Assertions.assertEquals(120, res1);
    }

    @Test
    public void factorialTest_nIs0_returns1() {
        Bornedan test = new Bornedan();
        long res = test.factorial(0);
        Assertions.assertEquals(1, res);
    }

    public void factorialTest_nIsMinus1_returns0() {
        Bornedan test = new Bornedan();
        long res = test.factorial(-1);
        Assertions.assertEquals(0, res);
    }
}
